﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using Oracle.ManagedDataAccess.Client;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using System.Security.Principal;
using System.Security.Authentication;
using System.Drawing.Printing;
using System.IO;

namespace EmpTraining
{
    public partial class ReportViewer : Form
    {
        public ReportViewer()
        {
            InitializeComponent();
        }
        private static DataTable GetData(string sqlCommand, string Num)
        {
            string varNum = Num;
            string connectionString = @"Data Source = GEI-SQL1.GEI.LOCAL,1433\sqlexpress; Initial Catalog = GEI_EMP_TRAINING; Integrated Security = SSPI";

            SqlConnection conn = new SqlConnection(connectionString);

            SqlCommand command = new SqlCommand(sqlCommand, conn);
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = command;

            DataTable table = new DataTable();
            table.Locale = System.Globalization.CultureInfo.InvariantCulture;
            command.Parameters.Add("@varNum", varNum);

            adapter.Fill(table);

            return table;
        }

        private void Form7_Load(object sender, EventArgs e)
        {
            //Initialize oracle connection                       

            SQLConn obj = new SQLConn();
            obj.OracleConnection();
            OracleDataReader reader = obj.ODataReader("Select EMP_NUM, NAME FROM GLOVIA_PROD.EMPLOYEE WHERE CREW_FLAG <> 'Y' order by NAME");

            reader.Read();

            while (reader.Read())
            {
                try
                {
                    ComboBox1.Items.Add(reader["EMP_NUM"] + "  |  " + reader["Name"]);

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }

            }

            obj.OracleCloseConnection();
            reader.Close();
           
            //END ORACLE CONNECTION

        }
        

        private void ComboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            string varDate = DateTime.Today.ToString();
            string varUser = Environment.UserName.ToString();
            string varNum = "";
            string varName = "";

            
            SQLConn obj = new SQLConn();
            obj.OracleConnection();
            OracleDataReader reader = obj.ODataReader("Select EMP_NUM, NAME FROM GLOVIA_PROD.EMPLOYEE WHERE CREW_FLAG <> 'Y' order by NAME");

            reader.Read();

            while (reader.Read())
            {
                try
                {
                    varNum = ComboBox1.SelectedItem.ToString();
                    varNum = varNum.Substring(0, 4);
                    varName = ComboBox1.SelectedItem.ToString();
                    varName = varName.Substring(9);

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }

            }
            obj.OracleCloseConnection();
            reader.Close();   
            

            
            obj.OpenConection();

            try
            {
                DataGridView1.DataSource = GetData("SELECT EMP_NUM, EMP_NAME, Job_Title, Create_Date, Question, JTScore, Created_By, " +
                    "Revisit FROM dbo.tblTraining_HDR WHERE EMP_NUM = @varNum", varNum);
                DataGridView2.DataSource = GetData("SELECT rec_id, EMP_NUM, EMP_NAME, Date_Created, Created_By, tailstretcherQ1, " +
                    "sawhelperQ1, presshelperQ1, pressteamQ1, pressteamQ2, pressteamQ3, pressteamQ4, pressteamQ5, pressteamQ6 " +
                    "FROM dbo.tblEXT_NEW_EMP WHERE EMP_NUM = @varNum", varNum);
                DataGridView3.DataSource = GetData("SELECT rec_id, EMP_NUM, EMP_NAME, Date_Created, Created_By, metalsawsQ1, custommachinesQ1, " +
                    "towQ1, craneQ1, fabpressQ1, combieQ1 FROM dbo.tblFAB_NEW_EMP WHERE EMP_NUM = @varNum", varNum);
                DataGridView4.DataSource = GetData("SELECT rec_id, EMP_NUM, EMP_NAME, Date_Created, Created_By, shipQ1 FROM dbo.tblSHIP_NEW_EMP " +
                    "WHERE EMP_NUM = @varNum", varNum);
                
            }
            catch (System.Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
            }
            obj.CloseConnection();
        }
                
    }
}
