﻿namespace EmpTraining
{
    partial class TrainingHDR
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.GroupBox3 = new System.Windows.Forms.GroupBox();
            this.Button12 = new System.Windows.Forms.Button();
            this.Button11 = new System.Windows.Forms.Button();
            this.Button9 = new System.Windows.Forms.Button();
            this.Button8 = new System.Windows.Forms.Button();
            this.Button7 = new System.Windows.Forms.Button();
            this.GroupBox2 = new System.Windows.Forms.GroupBox();
            this.Button10 = new System.Windows.Forms.Button();
            this.Button4 = new System.Windows.Forms.Button();
            this.Button6 = new System.Windows.Forms.Button();
            this.DataGridView1 = new System.Windows.Forms.DataGridView();
            this.questionDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.JTScore = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.Revisit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tblJobTitleQuesXREFBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gEIEMPTRAININGDataSetBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gEI_EMP_TRAININGDataSet = new EmpTraining.GEI_EMP_TRAININGDataSet();
            this.TblTitle_WI_XREFDataGridView = new System.Windows.Forms.DataGridView();
            this.wIRequiredDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tblTitleWIXREFBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.Button1 = new System.Windows.Forms.Button();
            this.Label2 = new System.Windows.Forms.Label();
            this.TextBox2 = new System.Windows.Forms.TextBox();
            this.GroupBox1 = new System.Windows.Forms.GroupBox();
            this.Button2 = new System.Windows.Forms.Button();
            this.Button5 = new System.Windows.Forms.Button();
            this.Button3 = new System.Windows.Forms.Button();
            this.ComboBox1 = new System.Windows.Forms.ComboBox();
            this.Label3 = new System.Windows.Forms.Label();
            this.ComboBox3 = new System.Windows.Forms.ComboBox();
            this.Label1 = new System.Windows.Forms.Label();
            this.tblTrainingHDRBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.tblTraining_HDRTableAdapter = new EmpTraining.GEI_EMP_TRAININGDataSetTableAdapters.tblTraining_HDRTableAdapter();
            this.tblJob_Title_Ques_XREFTableAdapter = new EmpTraining.GEI_EMP_TRAININGDataSetTableAdapters.tblJob_Title_Ques_XREFTableAdapter();
            this.tblTitle_WI_XREFTableAdapter = new EmpTraining.GEI_EMP_TRAININGDataSetTableAdapters.tblTitle_WI_XREFTableAdapter();
            this.printDocument1 = new System.Drawing.Printing.PrintDocument();
            this.printDocument2 = new System.Drawing.Printing.PrintDocument();
            this.GroupBox3.SuspendLayout();
            this.GroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblJobTitleQuesXREFBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gEIEMPTRAININGDataSetBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gEI_EMP_TRAININGDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TblTitle_WI_XREFDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblTitleWIXREFBindingSource)).BeginInit();
            this.GroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tblTrainingHDRBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // GroupBox3
            // 
            this.GroupBox3.Controls.Add(this.Button12);
            this.GroupBox3.Controls.Add(this.Button11);
            this.GroupBox3.Controls.Add(this.Button9);
            this.GroupBox3.Controls.Add(this.Button8);
            this.GroupBox3.Controls.Add(this.Button7);
            this.GroupBox3.Location = new System.Drawing.Point(795, 331);
            this.GroupBox3.Name = "GroupBox3";
            this.GroupBox3.Size = new System.Drawing.Size(169, 256);
            this.GroupBox3.TabIndex = 65;
            this.GroupBox3.TabStop = false;
            this.GroupBox3.Text = "Menu";
            // 
            // Button12
            // 
            this.Button12.Location = new System.Drawing.Point(9, 147);
            this.Button12.Name = "Button12";
            this.Button12.Size = new System.Drawing.Size(146, 38);
            this.Button12.TabIndex = 62;
            this.Button12.Text = "Print Coaching ";
            this.Button12.UseVisualStyleBackColor = true;
            this.Button12.Click += new System.EventHandler(this.Button12_Click);
            // 
            // Button11
            // 
            this.Button11.Location = new System.Drawing.Point(10, 103);
            this.Button11.Name = "Button11";
            this.Button11.Size = new System.Drawing.Size(146, 38);
            this.Button11.TabIndex = 61;
            this.Button11.Text = "Approved Requests";
            this.Button11.UseVisualStyleBackColor = true;
            this.Button11.Click += new System.EventHandler(this.Button11_Click);
            // 
            // Button9
            // 
            this.Button9.Location = new System.Drawing.Point(10, 15);
            this.Button9.Name = "Button9";
            this.Button9.Size = new System.Drawing.Size(146, 38);
            this.Button9.TabIndex = 53;
            this.Button9.Text = "Report Viewer";
            this.Button9.UseVisualStyleBackColor = true;
            this.Button9.Click += new System.EventHandler(this.Button9_Click);
            // 
            // Button8
            // 
            this.Button8.Location = new System.Drawing.Point(10, 59);
            this.Button8.Name = "Button8";
            this.Button8.Size = new System.Drawing.Size(146, 38);
            this.Button8.TabIndex = 52;
            this.Button8.Text = "Print WI Required";
            this.Button8.UseVisualStyleBackColor = true;
            this.Button8.Click += new System.EventHandler(this.Button8_Click);
            // 
            // Button7
            // 
            this.Button7.Location = new System.Drawing.Point(10, 191);
            this.Button7.Name = "Button7";
            this.Button7.Size = new System.Drawing.Size(146, 38);
            this.Button7.TabIndex = 51;
            this.Button7.Text = "Exit";
            this.Button7.UseVisualStyleBackColor = true;
            this.Button7.Click += new System.EventHandler(this.Button7_Click);
            // 
            // GroupBox2
            // 
            this.GroupBox2.Controls.Add(this.Button10);
            this.GroupBox2.Controls.Add(this.Button4);
            this.GroupBox2.Controls.Add(this.Button6);
            this.GroupBox2.Location = new System.Drawing.Point(795, 168);
            this.GroupBox2.Name = "GroupBox2";
            this.GroupBox2.Size = new System.Drawing.Size(169, 157);
            this.GroupBox2.TabIndex = 64;
            this.GroupBox2.TabStop = false;
            this.GroupBox2.Text = "Press and Vehicle Centers";
            // 
            // Button10
            // 
            this.Button10.Location = new System.Drawing.Point(10, 20);
            this.Button10.Name = "Button10";
            this.Button10.Size = new System.Drawing.Size(145, 38);
            this.Button10.TabIndex = 56;
            this.Button10.Text = "Press Team";
            this.Button10.UseVisualStyleBackColor = true;
            this.Button10.Click += new System.EventHandler(this.Button10_Click);
            // 
            // Button4
            // 
            this.Button4.Location = new System.Drawing.Point(10, 64);
            this.Button4.Name = "Button4";
            this.Button4.Size = new System.Drawing.Size(145, 38);
            this.Button4.TabIndex = 43;
            this.Button4.Text = "Fab Press";
            this.Button4.UseVisualStyleBackColor = true;
            this.Button4.Click += new System.EventHandler(this.Button4_Click);
            // 
            // Button6
            // 
            this.Button6.Location = new System.Drawing.Point(10, 108);
            this.Button6.Name = "Button6";
            this.Button6.Size = new System.Drawing.Size(145, 38);
            this.Button6.TabIndex = 48;
            this.Button6.Text = "Tow Motor\r\nCranes\r\n";
            this.Button6.UseVisualStyleBackColor = true;
            this.Button6.Click += new System.EventHandler(this.Button6_Click);
            // 
            // DataGridView1
            // 
            this.DataGridView1.AllowUserToAddRows = false;
            this.DataGridView1.AutoGenerateColumns = false;
            this.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.questionDataGridViewTextBoxColumn,
            this.JTScore,
            this.Revisit});
            this.DataGridView1.DataSource = this.tblJobTitleQuesXREFBindingSource;
            this.DataGridView1.Location = new System.Drawing.Point(170, 43);
            this.DataGridView1.Name = "DataGridView1";
            this.DataGridView1.Size = new System.Drawing.Size(619, 473);
            this.DataGridView1.TabIndex = 63;
            // 
            // questionDataGridViewTextBoxColumn
            // 
            this.questionDataGridViewTextBoxColumn.DataPropertyName = "Question";
            this.questionDataGridViewTextBoxColumn.HeaderText = "Question";
            this.questionDataGridViewTextBoxColumn.Name = "questionDataGridViewTextBoxColumn";
            this.questionDataGridViewTextBoxColumn.Width = 350;
            // 
            // JTScore
            // 
            this.JTScore.HeaderText = "JTScore";
            this.JTScore.Items.AddRange(new object[] {
            "Needs Training",
            "Adequate",
            "Proficient"});
            this.JTScore.Name = "JTScore";
            this.JTScore.Width = 140;
            // 
            // Revisit
            // 
            this.Revisit.DataPropertyName = "Revisit";
            this.Revisit.HeaderText = "Revisit";
            this.Revisit.Name = "Revisit";
            // 
            // tblJobTitleQuesXREFBindingSource
            // 
            this.tblJobTitleQuesXREFBindingSource.DataMember = "tblJob_Title_Ques_XREF";
            this.tblJobTitleQuesXREFBindingSource.DataSource = this.gEIEMPTRAININGDataSetBindingSource;
            // 
            // gEIEMPTRAININGDataSetBindingSource
            // 
            this.gEIEMPTRAININGDataSetBindingSource.DataSource = this.gEI_EMP_TRAININGDataSet;
            this.gEIEMPTRAININGDataSetBindingSource.Position = 0;
            // 
            // gEI_EMP_TRAININGDataSet
            // 
            this.gEI_EMP_TRAININGDataSet.DataSetName = "GEI_EMP_TRAININGDataSet";
            this.gEI_EMP_TRAININGDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // TblTitle_WI_XREFDataGridView
            // 
            this.TblTitle_WI_XREFDataGridView.AllowUserToAddRows = false;
            this.TblTitle_WI_XREFDataGridView.AllowUserToDeleteRows = false;
            this.TblTitle_WI_XREFDataGridView.AutoGenerateColumns = false;
            this.TblTitle_WI_XREFDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.TblTitle_WI_XREFDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.wIRequiredDataGridViewTextBoxColumn});
            this.TblTitle_WI_XREFDataGridView.DataSource = this.tblTitleWIXREFBindingSource;
            this.TblTitle_WI_XREFDataGridView.Location = new System.Drawing.Point(12, 17);
            this.TblTitle_WI_XREFDataGridView.Name = "TblTitle_WI_XREFDataGridView";
            this.TblTitle_WI_XREFDataGridView.ReadOnly = true;
            this.TblTitle_WI_XREFDataGridView.Size = new System.Drawing.Size(152, 570);
            this.TblTitle_WI_XREFDataGridView.TabIndex = 62;
            // 
            // wIRequiredDataGridViewTextBoxColumn
            // 
            this.wIRequiredDataGridViewTextBoxColumn.DataPropertyName = "WI_Required";
            this.wIRequiredDataGridViewTextBoxColumn.HeaderText = "WI_Required";
            this.wIRequiredDataGridViewTextBoxColumn.Name = "wIRequiredDataGridViewTextBoxColumn";
            this.wIRequiredDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // tblTitleWIXREFBindingSource
            // 
            this.tblTitleWIXREFBindingSource.DataMember = "tblTitle_WI_XREF";
            this.tblTitleWIXREFBindingSource.DataSource = this.gEIEMPTRAININGDataSetBindingSource;
            // 
            // Button1
            // 
            this.Button1.Location = new System.Drawing.Point(170, 529);
            this.Button1.Name = "Button1";
            this.Button1.Size = new System.Drawing.Size(619, 55);
            this.Button1.TabIndex = 61;
            this.Button1.Text = "Submit";
            this.Button1.UseVisualStyleBackColor = true;
            this.Button1.Click += new System.EventHandler(this.Button1_Click);
            // 
            // Label2
            // 
            this.Label2.AutoSize = true;
            this.Label2.Location = new System.Drawing.Point(505, 1);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(256, 13);
            this.Label2.TabIndex = 72;
            this.Label2.Text = "Revisit - Yes/No w/ Brief Explanation (255 char max)";
            // 
            // TextBox2
            // 
            this.TextBox2.Location = new System.Drawing.Point(486, 17);
            this.TextBox2.Name = "TextBox2";
            this.TextBox2.Size = new System.Drawing.Size(303, 20);
            this.TextBox2.TabIndex = 71;
            // 
            // GroupBox1
            // 
            this.GroupBox1.Controls.Add(this.Button2);
            this.GroupBox1.Controls.Add(this.Button5);
            this.GroupBox1.Controls.Add(this.Button3);
            this.GroupBox1.Location = new System.Drawing.Point(795, 9);
            this.GroupBox1.Name = "GroupBox1";
            this.GroupBox1.Size = new System.Drawing.Size(169, 157);
            this.GroupBox1.TabIndex = 70;
            this.GroupBox1.TabStop = false;
            this.GroupBox1.Text = "Training Centers";
            // 
            // Button2
            // 
            this.Button2.Location = new System.Drawing.Point(10, 20);
            this.Button2.Name = "Button2";
            this.Button2.Size = new System.Drawing.Size(146, 38);
            this.Button2.TabIndex = 41;
            this.Button2.Text = "Extrusions Training Center";
            this.Button2.UseVisualStyleBackColor = true;
            this.Button2.Click += new System.EventHandler(this.Button2_Click);
            // 
            // Button5
            // 
            this.Button5.Location = new System.Drawing.Point(10, 64);
            this.Button5.Name = "Button5";
            this.Button5.Size = new System.Drawing.Size(146, 39);
            this.Button5.TabIndex = 47;
            this.Button5.Text = "Fab Training Center\r\n";
            this.Button5.UseVisualStyleBackColor = true;
            this.Button5.Click += new System.EventHandler(this.Button5_Click);
            // 
            // Button3
            // 
            this.Button3.Location = new System.Drawing.Point(10, 109);
            this.Button3.Name = "Button3";
            this.Button3.Size = new System.Drawing.Size(146, 39);
            this.Button3.TabIndex = 42;
            this.Button3.Text = "Shipping Training Center";
            this.Button3.UseVisualStyleBackColor = true;
            this.Button3.Click += new System.EventHandler(this.Button3_Click);
            // 
            // ComboBox1
            // 
            this.ComboBox1.FormattingEnabled = true;
            this.ComboBox1.Location = new System.Drawing.Point(170, 16);
            this.ComboBox1.Name = "ComboBox1";
            this.ComboBox1.Size = new System.Drawing.Size(186, 21);
            this.ComboBox1.TabIndex = 69;
            this.ComboBox1.SelectedIndexChanged += new System.EventHandler(this.ComboBox1_SelectedIndexChanged);
            // 
            // Label3
            // 
            this.Label3.AutoSize = true;
            this.Label3.Location = new System.Drawing.Point(395, 0);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(47, 13);
            this.Label3.TabIndex = 68;
            this.Label3.Text = "Job Title";
            // 
            // ComboBox3
            // 
            this.ComboBox3.FormattingEnabled = true;
            this.ComboBox3.Location = new System.Drawing.Point(363, 16);
            this.ComboBox3.Name = "ComboBox3";
            this.ComboBox3.Size = new System.Drawing.Size(117, 21);
            this.ComboBox3.TabIndex = 67;
            this.ComboBox3.SelectedIndexChanged += new System.EventHandler(this.ComboBox3_SelectedIndexChanged);
            // 
            // Label1
            // 
            this.Label1.AutoSize = true;
            this.Label1.Location = new System.Drawing.Point(188, 0);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(127, 13);
            this.Label1.TabIndex = 66;
            this.Label1.Text = "Employee Badge Number";
            // 
            // tblTrainingHDRBindingSource
            // 
            this.tblTrainingHDRBindingSource.DataMember = "tblTraining_HDR";
            this.tblTrainingHDRBindingSource.DataSource = this.gEI_EMP_TRAININGDataSet;
            // 
            // tblTraining_HDRTableAdapter
            // 
            this.tblTraining_HDRTableAdapter.ClearBeforeFill = true;
            // 
            // tblJob_Title_Ques_XREFTableAdapter
            // 
            this.tblJob_Title_Ques_XREFTableAdapter.ClearBeforeFill = true;
            // 
            // tblTitle_WI_XREFTableAdapter
            // 
            this.tblTitle_WI_XREFTableAdapter.ClearBeforeFill = true;
            // 
            // printDocument1
            // 
            this.printDocument1.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.PrintDocument1_PrintPage);
            // 
            // printDocument2
            // 
            this.printDocument2.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.PrintDocument2_PrintPage);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(973, 596);
            this.Controls.Add(this.Label2);
            this.Controls.Add(this.TextBox2);
            this.Controls.Add(this.GroupBox1);
            this.Controls.Add(this.ComboBox1);
            this.Controls.Add(this.Label3);
            this.Controls.Add(this.ComboBox3);
            this.Controls.Add(this.Label1);
            this.Controls.Add(this.GroupBox3);
            this.Controls.Add(this.GroupBox2);
            this.Controls.Add(this.DataGridView1);
            this.Controls.Add(this.TblTitle_WI_XREFDataGridView);
            this.Controls.Add(this.Button1);
            this.Name = "Form1";
            this.Text = "Employee Training";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.GroupBox3.ResumeLayout(false);
            this.GroupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblJobTitleQuesXREFBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gEIEMPTRAININGDataSetBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gEI_EMP_TRAININGDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TblTitle_WI_XREFDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblTitleWIXREFBindingSource)).EndInit();
            this.GroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tblTrainingHDRBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.GroupBox GroupBox3;
        internal System.Windows.Forms.Button Button12;
        internal System.Windows.Forms.Button Button11;
        internal System.Windows.Forms.Button Button9;
        internal System.Windows.Forms.Button Button8;
        internal System.Windows.Forms.Button Button7;
        internal System.Windows.Forms.GroupBox GroupBox2;
        internal System.Windows.Forms.Button Button10;
        internal System.Windows.Forms.Button Button4;
        internal System.Windows.Forms.Button Button6;
        internal System.Windows.Forms.DataGridView DataGridView1;
        internal System.Windows.Forms.DataGridView TblTitle_WI_XREFDataGridView;
        internal System.Windows.Forms.Button Button1;
        internal System.Windows.Forms.Label Label2;
        internal System.Windows.Forms.TextBox TextBox2;
        internal System.Windows.Forms.GroupBox GroupBox1;
        internal System.Windows.Forms.Button Button2;
        internal System.Windows.Forms.Button Button5;
        internal System.Windows.Forms.Button Button3;
        internal System.Windows.Forms.ComboBox ComboBox1;
        internal System.Windows.Forms.Label Label3;
        internal System.Windows.Forms.ComboBox ComboBox3;
        internal System.Windows.Forms.Label Label1;
        private GEI_EMP_TRAININGDataSet gEI_EMP_TRAININGDataSet;
        private System.Windows.Forms.BindingSource tblTrainingHDRBindingSource;
        private GEI_EMP_TRAININGDataSetTableAdapters.tblTraining_HDRTableAdapter tblTraining_HDRTableAdapter;
        private System.Windows.Forms.BindingSource gEIEMPTRAININGDataSetBindingSource;
        private System.Windows.Forms.BindingSource tblJobTitleQuesXREFBindingSource;
        private GEI_EMP_TRAININGDataSetTableAdapters.tblJob_Title_Ques_XREFTableAdapter tblJob_Title_Ques_XREFTableAdapter;
        private System.Windows.Forms.BindingSource tblTitleWIXREFBindingSource;
        private GEI_EMP_TRAININGDataSetTableAdapters.tblTitle_WI_XREFTableAdapter tblTitle_WI_XREFTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn wIRequiredDataGridViewTextBoxColumn;
        private System.Drawing.Printing.PrintDocument printDocument1;
        private System.Drawing.Printing.PrintDocument printDocument2;
        private System.Windows.Forms.DataGridViewTextBoxColumn questionDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn JTScore;
        private System.Windows.Forms.DataGridViewTextBoxColumn Revisit;
    }
}

