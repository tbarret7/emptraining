﻿namespace EmpTraining
{
    partial class ReportViewer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.Label6 = new System.Windows.Forms.Label();
            this.DataGridView4 = new System.Windows.Forms.DataGridView();
            this.dateCreatedDataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.createdByDataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.shipQ1DataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.tblSHIPNEWEMPBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gEI_EMP_TRAININGDataSet = new EmpTraining.GEI_EMP_TRAININGDataSet();
            this.DataGridView3 = new System.Windows.Forms.DataGridView();
            this.dateCreatedDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.custommachinesQ1 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.metalsawsQ1DataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.towQ1DataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.craneQ1DataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.fabpressQ1DataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.combieQ1DataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.createdByDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tblFABNEWEMPBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.DataGridView2 = new System.Windows.Forms.DataGridView();
            this.dateCreatedDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tailstretcherQ1 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.sawhelperQ1 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.presshelperQ1 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.pressteamQ1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pressteamQ2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pressteamQ3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pressteamQ4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pressteamQ5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pressteamQ6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.createdByDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tblEXTNEWEMPBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.DataGridView1 = new System.Windows.Forms.DataGridView();
            this.jobTitleDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.createDateDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.questionDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.JTScore = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Revisit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tblTrainingHDRBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.ComboBox1 = new System.Windows.Forms.ComboBox();
            this.Label1 = new System.Windows.Forms.Label();
            this.tblTraining_HDRTableAdapter = new EmpTraining.GEI_EMP_TRAININGDataSetTableAdapters.tblTraining_HDRTableAdapter();
            this.tblEXT_NEW_EMPTableAdapter = new EmpTraining.GEI_EMP_TRAININGDataSetTableAdapters.tblEXT_NEW_EMPTableAdapter();
            this.tblFAB_NEW_EMPTableAdapter = new EmpTraining.GEI_EMP_TRAININGDataSetTableAdapters.tblFAB_NEW_EMPTableAdapter();
            this.tblSHIP_NEW_EMPTableAdapter = new EmpTraining.GEI_EMP_TRAININGDataSetTableAdapters.tblSHIP_NEW_EMPTableAdapter();
            this.Label5 = new System.Windows.Forms.Label();
            this.Label2 = new System.Windows.Forms.Label();
            this.Label3 = new System.Windows.Forms.Label();
            this.Label4 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridView4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblSHIPNEWEMPBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gEI_EMP_TRAININGDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblFABNEWEMPBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblEXTNEWEMPBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblTrainingHDRBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // Label6
            // 
            this.Label6.AutoSize = true;
            this.Label6.Location = new System.Drawing.Point(428, 16);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(140, 13);
            this.Label6.TabIndex = 19;
            this.Label6.Text = "These tables are Read-only.";
            // 
            // DataGridView4
            // 
            this.DataGridView4.AllowUserToAddRows = false;
            this.DataGridView4.AllowUserToDeleteRows = false;
            this.DataGridView4.AutoGenerateColumns = false;
            this.DataGridView4.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridView4.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dateCreatedDataGridViewTextBoxColumn2,
            this.createdByDataGridViewTextBoxColumn2,
            this.shipQ1DataGridViewCheckBoxColumn});
            this.DataGridView4.DataSource = this.tblSHIPNEWEMPBindingSource;
            this.DataGridView4.Location = new System.Drawing.Point(12, 625);
            this.DataGridView4.Name = "DataGridView4";
            this.DataGridView4.ReadOnly = true;
            this.DataGridView4.Size = new System.Drawing.Size(945, 161);
            this.DataGridView4.TabIndex = 18;
            // 
            // dateCreatedDataGridViewTextBoxColumn2
            // 
            this.dateCreatedDataGridViewTextBoxColumn2.DataPropertyName = "Date_Created";
            this.dateCreatedDataGridViewTextBoxColumn2.HeaderText = "Date_Created";
            this.dateCreatedDataGridViewTextBoxColumn2.Name = "dateCreatedDataGridViewTextBoxColumn2";
            this.dateCreatedDataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // createdByDataGridViewTextBoxColumn2
            // 
            this.createdByDataGridViewTextBoxColumn2.DataPropertyName = "Created_By";
            this.createdByDataGridViewTextBoxColumn2.HeaderText = "Created_By";
            this.createdByDataGridViewTextBoxColumn2.Name = "createdByDataGridViewTextBoxColumn2";
            this.createdByDataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // shipQ1DataGridViewCheckBoxColumn
            // 
            this.shipQ1DataGridViewCheckBoxColumn.DataPropertyName = "shipQ1";
            this.shipQ1DataGridViewCheckBoxColumn.HeaderText = "shipQ1";
            this.shipQ1DataGridViewCheckBoxColumn.Name = "shipQ1DataGridViewCheckBoxColumn";
            this.shipQ1DataGridViewCheckBoxColumn.ReadOnly = true;
            // 
            // tblSHIPNEWEMPBindingSource
            // 
            this.tblSHIPNEWEMPBindingSource.DataMember = "tblSHIP_NEW_EMP";
            this.tblSHIPNEWEMPBindingSource.DataSource = this.gEI_EMP_TRAININGDataSet;
            // 
            // gEI_EMP_TRAININGDataSet
            // 
            this.gEI_EMP_TRAININGDataSet.DataSetName = "GEI_EMP_TRAININGDataSet";
            this.gEI_EMP_TRAININGDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // DataGridView3
            // 
            this.DataGridView3.AllowUserToAddRows = false;
            this.DataGridView3.AllowUserToDeleteRows = false;
            this.DataGridView3.AutoGenerateColumns = false;
            this.DataGridView3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridView3.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dateCreatedDataGridViewTextBoxColumn1,
            this.custommachinesQ1,
            this.metalsawsQ1DataGridViewCheckBoxColumn,
            this.towQ1DataGridViewCheckBoxColumn,
            this.craneQ1DataGridViewCheckBoxColumn,
            this.fabpressQ1DataGridViewCheckBoxColumn,
            this.combieQ1DataGridViewCheckBoxColumn,
            this.createdByDataGridViewTextBoxColumn1});
            this.DataGridView3.DataSource = this.tblFABNEWEMPBindingSource;
            this.DataGridView3.Location = new System.Drawing.Point(12, 428);
            this.DataGridView3.Name = "DataGridView3";
            this.DataGridView3.ReadOnly = true;
            this.DataGridView3.Size = new System.Drawing.Size(945, 170);
            this.DataGridView3.TabIndex = 17;
            // 
            // dateCreatedDataGridViewTextBoxColumn1
            // 
            this.dateCreatedDataGridViewTextBoxColumn1.DataPropertyName = "Date_Created";
            this.dateCreatedDataGridViewTextBoxColumn1.HeaderText = "Date_Created";
            this.dateCreatedDataGridViewTextBoxColumn1.Name = "dateCreatedDataGridViewTextBoxColumn1";
            this.dateCreatedDataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // custommachinesQ1
            // 
            this.custommachinesQ1.DataPropertyName = "custommachinesQ1";
            this.custommachinesQ1.HeaderText = "custommachinesQ1";
            this.custommachinesQ1.Name = "custommachinesQ1";
            this.custommachinesQ1.ReadOnly = true;
            // 
            // metalsawsQ1DataGridViewCheckBoxColumn
            // 
            this.metalsawsQ1DataGridViewCheckBoxColumn.DataPropertyName = "metalsawsQ1";
            this.metalsawsQ1DataGridViewCheckBoxColumn.HeaderText = "metalsawsQ1";
            this.metalsawsQ1DataGridViewCheckBoxColumn.Name = "metalsawsQ1DataGridViewCheckBoxColumn";
            this.metalsawsQ1DataGridViewCheckBoxColumn.ReadOnly = true;
            // 
            // towQ1DataGridViewCheckBoxColumn
            // 
            this.towQ1DataGridViewCheckBoxColumn.DataPropertyName = "towQ1";
            this.towQ1DataGridViewCheckBoxColumn.HeaderText = "towQ1";
            this.towQ1DataGridViewCheckBoxColumn.Name = "towQ1DataGridViewCheckBoxColumn";
            this.towQ1DataGridViewCheckBoxColumn.ReadOnly = true;
            // 
            // craneQ1DataGridViewCheckBoxColumn
            // 
            this.craneQ1DataGridViewCheckBoxColumn.DataPropertyName = "craneQ1";
            this.craneQ1DataGridViewCheckBoxColumn.HeaderText = "craneQ1";
            this.craneQ1DataGridViewCheckBoxColumn.Name = "craneQ1DataGridViewCheckBoxColumn";
            this.craneQ1DataGridViewCheckBoxColumn.ReadOnly = true;
            // 
            // fabpressQ1DataGridViewCheckBoxColumn
            // 
            this.fabpressQ1DataGridViewCheckBoxColumn.DataPropertyName = "fabpressQ1";
            this.fabpressQ1DataGridViewCheckBoxColumn.HeaderText = "fabpressQ1";
            this.fabpressQ1DataGridViewCheckBoxColumn.Name = "fabpressQ1DataGridViewCheckBoxColumn";
            this.fabpressQ1DataGridViewCheckBoxColumn.ReadOnly = true;
            // 
            // combieQ1DataGridViewCheckBoxColumn
            // 
            this.combieQ1DataGridViewCheckBoxColumn.DataPropertyName = "combieQ1";
            this.combieQ1DataGridViewCheckBoxColumn.HeaderText = "combieQ1";
            this.combieQ1DataGridViewCheckBoxColumn.Name = "combieQ1DataGridViewCheckBoxColumn";
            this.combieQ1DataGridViewCheckBoxColumn.ReadOnly = true;
            // 
            // createdByDataGridViewTextBoxColumn1
            // 
            this.createdByDataGridViewTextBoxColumn1.DataPropertyName = "Created_By";
            this.createdByDataGridViewTextBoxColumn1.HeaderText = "Created_By";
            this.createdByDataGridViewTextBoxColumn1.Name = "createdByDataGridViewTextBoxColumn1";
            this.createdByDataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // tblFABNEWEMPBindingSource
            // 
            this.tblFABNEWEMPBindingSource.DataMember = "tblFAB_NEW_EMP";
            this.tblFABNEWEMPBindingSource.DataSource = this.gEI_EMP_TRAININGDataSet;
            // 
            // DataGridView2
            // 
            this.DataGridView2.AllowUserToAddRows = false;
            this.DataGridView2.AllowUserToDeleteRows = false;
            this.DataGridView2.AutoGenerateColumns = false;
            this.DataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridView2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dateCreatedDataGridViewTextBoxColumn,
            this.tailstretcherQ1,
            this.sawhelperQ1,
            this.presshelperQ1,
            this.pressteamQ1,
            this.pressteamQ2,
            this.pressteamQ3,
            this.pressteamQ4,
            this.pressteamQ5,
            this.pressteamQ6,
            this.createdByDataGridViewTextBoxColumn});
            this.DataGridView2.DataSource = this.tblEXTNEWEMPBindingSource;
            this.DataGridView2.Location = new System.Drawing.Point(12, 238);
            this.DataGridView2.Name = "DataGridView2";
            this.DataGridView2.ReadOnly = true;
            this.DataGridView2.Size = new System.Drawing.Size(945, 156);
            this.DataGridView2.TabIndex = 16;
            // 
            // dateCreatedDataGridViewTextBoxColumn
            // 
            this.dateCreatedDataGridViewTextBoxColumn.DataPropertyName = "Date_Created";
            this.dateCreatedDataGridViewTextBoxColumn.HeaderText = "Date_Created";
            this.dateCreatedDataGridViewTextBoxColumn.Name = "dateCreatedDataGridViewTextBoxColumn";
            this.dateCreatedDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // tailstretcherQ1
            // 
            this.tailstretcherQ1.DataPropertyName = "tailstretcherQ1";
            this.tailstretcherQ1.HeaderText = "tailstretcherQ1";
            this.tailstretcherQ1.Name = "tailstretcherQ1";
            this.tailstretcherQ1.ReadOnly = true;
            // 
            // sawhelperQ1
            // 
            this.sawhelperQ1.DataPropertyName = "sawhelperQ1";
            this.sawhelperQ1.HeaderText = "sawhelperQ1";
            this.sawhelperQ1.Name = "sawhelperQ1";
            this.sawhelperQ1.ReadOnly = true;
            // 
            // presshelperQ1
            // 
            this.presshelperQ1.DataPropertyName = "presshelperQ1";
            this.presshelperQ1.HeaderText = "presshelperQ1";
            this.presshelperQ1.Name = "presshelperQ1";
            this.presshelperQ1.ReadOnly = true;
            // 
            // pressteamQ1
            // 
            this.pressteamQ1.DataPropertyName = "pressteamQ1";
            this.pressteamQ1.HeaderText = "pressteamQ1";
            this.pressteamQ1.Name = "pressteamQ1";
            this.pressteamQ1.ReadOnly = true;
            // 
            // pressteamQ2
            // 
            this.pressteamQ2.DataPropertyName = "pressteamQ2";
            this.pressteamQ2.HeaderText = "pressteamQ2";
            this.pressteamQ2.Name = "pressteamQ2";
            this.pressteamQ2.ReadOnly = true;
            // 
            // pressteamQ3
            // 
            this.pressteamQ3.DataPropertyName = "pressteamQ3";
            this.pressteamQ3.HeaderText = "pressteamQ3";
            this.pressteamQ3.Name = "pressteamQ3";
            this.pressteamQ3.ReadOnly = true;
            // 
            // pressteamQ4
            // 
            this.pressteamQ4.DataPropertyName = "pressteamQ4";
            this.pressteamQ4.HeaderText = "pressteamQ4";
            this.pressteamQ4.Name = "pressteamQ4";
            this.pressteamQ4.ReadOnly = true;
            // 
            // pressteamQ5
            // 
            this.pressteamQ5.DataPropertyName = "pressteamQ5";
            this.pressteamQ5.HeaderText = "pressteamQ5";
            this.pressteamQ5.Name = "pressteamQ5";
            this.pressteamQ5.ReadOnly = true;
            // 
            // pressteamQ6
            // 
            this.pressteamQ6.DataPropertyName = "pressteamQ6";
            this.pressteamQ6.HeaderText = "pressteamQ6";
            this.pressteamQ6.Name = "pressteamQ6";
            this.pressteamQ6.ReadOnly = true;
            // 
            // createdByDataGridViewTextBoxColumn
            // 
            this.createdByDataGridViewTextBoxColumn.DataPropertyName = "Created_By";
            this.createdByDataGridViewTextBoxColumn.HeaderText = "Created_By";
            this.createdByDataGridViewTextBoxColumn.Name = "createdByDataGridViewTextBoxColumn";
            this.createdByDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // tblEXTNEWEMPBindingSource
            // 
            this.tblEXTNEWEMPBindingSource.DataMember = "tblEXT_NEW_EMP";
            this.tblEXTNEWEMPBindingSource.DataSource = this.gEI_EMP_TRAININGDataSet;
            // 
            // DataGridView1
            // 
            this.DataGridView1.AllowUserToAddRows = false;
            this.DataGridView1.AllowUserToDeleteRows = false;
            this.DataGridView1.AutoGenerateColumns = false;
            this.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.jobTitleDataGridViewTextBoxColumn,
            this.createDateDataGridViewTextBoxColumn,
            this.questionDataGridViewTextBoxColumn,
            this.JTScore,
            this.Revisit});
            this.DataGridView1.DataSource = this.tblTrainingHDRBindingSource;
            this.DataGridView1.Location = new System.Drawing.Point(12, 58);
            this.DataGridView1.Name = "DataGridView1";
            this.DataGridView1.ReadOnly = true;
            this.DataGridView1.Size = new System.Drawing.Size(945, 153);
            this.DataGridView1.TabIndex = 15;
            // 
            // jobTitleDataGridViewTextBoxColumn
            // 
            this.jobTitleDataGridViewTextBoxColumn.DataPropertyName = "Job_Title";
            this.jobTitleDataGridViewTextBoxColumn.HeaderText = "Job_Title";
            this.jobTitleDataGridViewTextBoxColumn.Name = "jobTitleDataGridViewTextBoxColumn";
            this.jobTitleDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // createDateDataGridViewTextBoxColumn
            // 
            this.createDateDataGridViewTextBoxColumn.DataPropertyName = "Create_Date";
            this.createDateDataGridViewTextBoxColumn.HeaderText = "Create_Date";
            this.createDateDataGridViewTextBoxColumn.Name = "createDateDataGridViewTextBoxColumn";
            this.createDateDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // questionDataGridViewTextBoxColumn
            // 
            this.questionDataGridViewTextBoxColumn.DataPropertyName = "Question";
            this.questionDataGridViewTextBoxColumn.HeaderText = "Question";
            this.questionDataGridViewTextBoxColumn.Name = "questionDataGridViewTextBoxColumn";
            this.questionDataGridViewTextBoxColumn.ReadOnly = true;
            this.questionDataGridViewTextBoxColumn.Width = 500;
            // 
            // JTScore
            // 
            this.JTScore.DataPropertyName = "JTScore";
            this.JTScore.HeaderText = "JTScore";
            this.JTScore.Name = "JTScore";
            this.JTScore.ReadOnly = true;
            this.JTScore.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // Revisit
            // 
            this.Revisit.DataPropertyName = "Revisit";
            this.Revisit.HeaderText = "Revisit";
            this.Revisit.Name = "Revisit";
            this.Revisit.ReadOnly = true;
            // 
            // tblTrainingHDRBindingSource
            // 
            this.tblTrainingHDRBindingSource.DataMember = "tblTraining_HDR";
            this.tblTrainingHDRBindingSource.DataSource = this.gEI_EMP_TRAININGDataSet;
            // 
            // ComboBox1
            // 
            this.ComboBox1.FormattingEnabled = true;
            this.ComboBox1.Location = new System.Drawing.Point(177, 13);
            this.ComboBox1.Name = "ComboBox1";
            this.ComboBox1.Size = new System.Drawing.Size(245, 21);
            this.ComboBox1.TabIndex = 14;
            this.ComboBox1.SelectedIndexChanged += new System.EventHandler(this.ComboBox1_SelectedIndexChanged);
            // 
            // Label1
            // 
            this.Label1.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.Label1.AutoSize = true;
            this.Label1.Location = new System.Drawing.Point(12, 16);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(159, 13);
            this.Label1.TabIndex = 20;
            this.Label1.Text = "Select Employee to view record:";
            // 
            // tblTraining_HDRTableAdapter
            // 
            this.tblTraining_HDRTableAdapter.ClearBeforeFill = true;
            // 
            // tblEXT_NEW_EMPTableAdapter
            // 
            this.tblEXT_NEW_EMPTableAdapter.ClearBeforeFill = true;
            // 
            // tblFAB_NEW_EMPTableAdapter
            // 
            this.tblFAB_NEW_EMPTableAdapter.ClearBeforeFill = true;
            // 
            // tblSHIP_NEW_EMPTableAdapter
            // 
            this.tblSHIP_NEW_EMPTableAdapter.ClearBeforeFill = true;
            // 
            // Label5
            // 
            this.Label5.AutoSize = true;
            this.Label5.Location = new System.Drawing.Point(12, 42);
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(72, 13);
            this.Label5.TabIndex = 21;
            this.Label5.Text = "Training HDR";
            // 
            // Label2
            // 
            this.Label2.AutoSize = true;
            this.Label2.Location = new System.Drawing.Point(12, 222);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(95, 13);
            this.Label2.TabIndex = 22;
            this.Label2.Text = "EXT Center Table ";
            // 
            // Label3
            // 
            this.Label3.AutoSize = true;
            this.Label3.Location = new System.Drawing.Point(12, 412);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(89, 13);
            this.Label3.TabIndex = 23;
            this.Label3.Text = "Fab Center Table";
            // 
            // Label4
            // 
            this.Label4.AutoSize = true;
            this.Label4.Location = new System.Drawing.Point(12, 609);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(92, 13);
            this.Label4.TabIndex = 24;
            this.Label4.Text = "Ship Center Table";
            // 
            // Form7
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(977, 806);
            this.Controls.Add(this.Label4);
            this.Controls.Add(this.Label3);
            this.Controls.Add(this.Label2);
            this.Controls.Add(this.Label5);
            this.Controls.Add(this.Label1);
            this.Controls.Add(this.Label6);
            this.Controls.Add(this.DataGridView4);
            this.Controls.Add(this.DataGridView3);
            this.Controls.Add(this.DataGridView2);
            this.Controls.Add(this.DataGridView1);
            this.Controls.Add(this.ComboBox1);
            this.Name = "Form7";
            this.Text = "Report Viewer";
            this.Load += new System.EventHandler(this.Form7_Load);
            ((System.ComponentModel.ISupportInitialize)(this.DataGridView4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblSHIPNEWEMPBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gEI_EMP_TRAININGDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblFABNEWEMPBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblEXTNEWEMPBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblTrainingHDRBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.Label Label6;
        internal System.Windows.Forms.DataGridView DataGridView4;
        internal System.Windows.Forms.DataGridView DataGridView3;
        internal System.Windows.Forms.DataGridView DataGridView2;
        internal System.Windows.Forms.DataGridView DataGridView1;
        internal System.Windows.Forms.ComboBox ComboBox1;
        internal System.Windows.Forms.Label Label1;
        private GEI_EMP_TRAININGDataSet gEI_EMP_TRAININGDataSet;
        private System.Windows.Forms.BindingSource tblTrainingHDRBindingSource;
        private GEI_EMP_TRAININGDataSetTableAdapters.tblTraining_HDRTableAdapter tblTraining_HDRTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn jobTitleDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn createDateDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn questionDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn JTScore;
        private System.Windows.Forms.DataGridViewTextBoxColumn Revisit;
        private System.Windows.Forms.BindingSource tblEXTNEWEMPBindingSource;
        private GEI_EMP_TRAININGDataSetTableAdapters.tblEXT_NEW_EMPTableAdapter tblEXT_NEW_EMPTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn dateCreatedDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn tailstretcherQ1;
        private System.Windows.Forms.DataGridViewCheckBoxColumn sawhelperQ1;
        private System.Windows.Forms.DataGridViewCheckBoxColumn presshelperQ1;
        private System.Windows.Forms.DataGridViewTextBoxColumn pressteamQ1;
        private System.Windows.Forms.DataGridViewTextBoxColumn pressteamQ2;
        private System.Windows.Forms.DataGridViewTextBoxColumn pressteamQ3;
        private System.Windows.Forms.DataGridViewTextBoxColumn pressteamQ4;
        private System.Windows.Forms.DataGridViewTextBoxColumn pressteamQ5;
        private System.Windows.Forms.DataGridViewTextBoxColumn pressteamQ6;
        private System.Windows.Forms.DataGridViewTextBoxColumn createdByDataGridViewTextBoxColumn;
        private System.Windows.Forms.BindingSource tblFABNEWEMPBindingSource;
        private GEI_EMP_TRAININGDataSetTableAdapters.tblFAB_NEW_EMPTableAdapter tblFAB_NEW_EMPTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn dateCreatedDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewCheckBoxColumn custommachinesQ1;
        private System.Windows.Forms.DataGridViewCheckBoxColumn metalsawsQ1DataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn towQ1DataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn craneQ1DataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn fabpressQ1DataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn combieQ1DataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn createdByDataGridViewTextBoxColumn1;
        private System.Windows.Forms.BindingSource tblSHIPNEWEMPBindingSource;
        private GEI_EMP_TRAININGDataSetTableAdapters.tblSHIP_NEW_EMPTableAdapter tblSHIP_NEW_EMPTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn dateCreatedDataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn createdByDataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewCheckBoxColumn shipQ1DataGridViewCheckBoxColumn;
        internal System.Windows.Forms.Label Label5;
        internal System.Windows.Forms.Label Label2;
        internal System.Windows.Forms.Label Label3;
        internal System.Windows.Forms.Label Label4;
    }
}