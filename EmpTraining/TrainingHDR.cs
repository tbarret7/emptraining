﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using Oracle.ManagedDataAccess.Client;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using System.Security.Principal;
using System.Security.Authentication;
using System.Drawing.Printing;
using System.IO;
using System.Security.Permissions;
using System.Threading;
using Microsoft.SqlServer.Server;


namespace EmpTraining
{
    public partial class TrainingHDR : Form
    {

        

        public TrainingHDR()
        {
            InitializeComponent();
        }
        public static bool IsInGroup(string ingroup)
        {
            string username = Environment.UserName;

            PrincipalContext domainctx = new PrincipalContext(ContextType.Domain, "Gei.Local");

            UserPrincipal userPrincipal =
                              UserPrincipal.FindByIdentity(domainctx, IdentityType.SamAccountName, username);

            bool isMember = userPrincipal.IsMemberOf(domainctx, IdentityType.Name, ingroup);

            return isMember;
        }
        private static DataTable GetData(string sqlCommand, string mytitle)
        {
            string jTitle = mytitle;
            string connectionString = @"Data Source = GEI-SQL1.GEI.LOCAL,1433\sqlexpress; Initial Catalog = GEI_EMP_TRAINING; Integrated Security = SSPI";

            SqlConnection conn = new SqlConnection(connectionString);
            
            SqlCommand command = new SqlCommand(sqlCommand, conn);
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = command;

            DataTable table = new DataTable();
            table.Locale = System.Globalization.CultureInfo.InvariantCulture;
            command.Parameters.Add("@jTitle", jTitle);

            adapter.Fill(table);

            return table;
        }


        private void Form1_Load(object sender, EventArgs e)
        {
            string jTitle = "";            

            SQLConn obj = new SQLConn();
            obj.OpenConection();
            SqlDataReader dr = obj.DataReader("Select Job_Title from tblJob_Title_Ques_XREF GROUP BY Job_Title");

            while (dr.Read())
            {
                ComboBox3.Items.Add(dr["Job_Title"].ToString());

            }

            obj.CloseConnection();
            

            //Initialize oracle connection
            
            obj.OracleConnection();
            OracleDataReader reader = obj.ODataReader("Select EMP_NUM, NAME FROM GLOVIA_PROD.EMPLOYEE WHERE CREW_FLAG <> 'Y' order by NAME");

            reader.Read();
            
            while (reader.Read())
            {
                try
                {
                    ComboBox1.Items.Add(reader["EMP_NUM"] + "  |  " + reader["Name"]);

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }

            }

            obj.OracleCloseConnection();
            reader.Close();
            

            //END ORACLE CONNECTION 

            //populate hidden fields

            //end population

        }

        private void ComboBox3_SelectedIndexChanged(object sender, EventArgs e)
        {
            string jTitle = "";
            jTitle = ComboBox3.Text.ToString();
            
            SQLConn obj = new SQLConn();
            obj.OpenConection();
            try
            {
                gEIEMPTRAININGDataSetBindingSource.DataSource = GetData("SELECT Job_Title, Question FROM dbo.tblJob_Title_Ques_XREF WHERE Job_Title = @jTitle", jTitle);
                DataGridView1.DataSource = gEIEMPTRAININGDataSetBindingSource;
                TblTitle_WI_XREFDataGridView.DataSource = GetData("SELECT Job_Title, WI_Required FROM dbo.tblTitle_WI_XREF WHERE Job_Title = @jTitle", jTitle);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
           
            obj.CloseConnection();
        }
        private void ComboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            string varNum = "";
            string varName = "";            

            SQLConn obj = new SQLConn();
            obj.OracleConnection();

            OracleDataReader reader = obj.ODataReader("Select EMP_NUM, NAME FROM GLOVIA_PROD.EMPLOYEE WHERE CREW_FLAG <> 'Y' order by NAME");

            reader.Read();

            while (reader.Read())
            {
                try
                {
                    varNum = ComboBox1.SelectedItem.ToString();
                    varNum = varNum.Substring(0, 4);
                    varName = ComboBox1.SelectedItem.ToString();
                    varName = varName.Substring(9);

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }

            }
            obj.OracleCloseConnection();
            reader.Close();
           
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            string varNum = "";
            string varName = "";            

            SQLConn obj = new SQLConn();
            obj.OracleConnection();
            OracleDataReader reader = obj.ODataReader("Select EMP_NUM, NAME FROM GLOVIA_PROD.EMPLOYEE WHERE CREW_FLAG <> 'Y' order by NAME");

            reader.Read();

            while (reader.Read())
            {
                try
                {
                    varNum = ComboBox1.SelectedItem.ToString();
                    varNum = varNum.Substring(0, 4);
                    varName = ComboBox1.SelectedItem.ToString();
                    varName = varName.Substring(9);

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }

            }
            obj.OracleCloseConnection();
            reader.Close();
            

            string jTitle = "";
            jTitle = ComboBox3.Text.ToString();
            string revisit = "";
            revisit = TextBox2.Text;
            string varUser = "";
            varUser = Environment.UserName.ToString();
            string varDate = DateTime.Today.ToString("MM/dd/yyyy HH:mm");
                      

            try
            {
               
                string jtscore = "";
                foreach (DataGridViewRow row in DataGridView1.Rows)
                {
                    jtscore = row.Cells["JTScore"].Value.ToString();
                                                           
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }

            try
            {
                foreach (DataGridViewRow rows in DataGridView1.Rows)
                {
                    string quest = "";
                    string jtscore = "";

                    quest = rows.Cells["QuestionDataGridViewTextBoxColumn"].Value.ToString();
                    jtscore = rows.Cells["JTScore"].Value.ToString();
                    string qry = "Insert into tblTraining_HDR (EMP_NUM, EMP_NAME, Job_Title, Create_Date, Question, JTScore, Created_By, Revisit) Select '" 
                    + varNum + "' as EMP_NUM_Ins, '"
                    + varName + "' as EMP_NAME_Ins, '" + jTitle + "' as Job_Title_Ins, '" + varDate + "' as Create_Date_Ins, '"
                    + quest + "' as Question_INS, '" + jtscore + "' as JTScore_INS, '"
                    + varUser + "' as Created_By_Ins, '" + revisit + "' as Revisit_Ins";

                    
                    obj.OpenConection();
                    
                    DataTable table1 = new DataTable();

                    obj.DataAdapter(qry).Fill(table1);

                    obj.CloseConnection();

                }
            }
            catch (Exception x)
            {
                MessageBox.Show(x.ToString());
            }
            MessageBox.Show("Employee record has been successfully updated.");

        }

        private void Button2_Click(object sender, EventArgs e)
        {
            string value = ComboBox1.Text; 
            EXTCenter frm2 = new EXTCenter(value);
            frm2.ShowDialog();
        }

        private void Button3_Click(object sender, EventArgs e)
        {
            string value = ComboBox1.Text;
            SHIPCenter frm3 = new SHIPCenter(value);
            frm3.ShowDialog();
        }

        private void Button4_Click(object sender, EventArgs e)
        {
            string value = ComboBox1.Text;
            FABPress frm4 = new FABPress(value);
            frm4.ShowDialog();
        }

        private void Button5_Click(object sender, EventArgs e)
        {
            string value = ComboBox1.Text;
            FABCenter frm5 = new FABCenter(value);
            frm5.ShowDialog();
        }

        private void Button6_Click(object sender, EventArgs e)
        {
            string value = ComboBox1.Text;
            HeavyMachinery frm6 = new HeavyMachinery(value);
            frm6.ShowDialog();
        }

        private void Button9_Click(object sender, EventArgs e)
        {            
            ReportViewer frm7 = new ReportViewer();
            frm7.ShowDialog();

        }

        private void Button10_Click(object sender, EventArgs e)
        {
            string value = ComboBox1.Text;
            PressTeam frm8 = new PressTeam(value);
            frm8.ShowDialog();

        }

        private void PrintDocument1_PrintPage(object sender, PrintPageEventArgs e)
        {
            string strFile = @"Y:\Production\Training\Employee_Training\coaching_sheet.xlsx";
            System.Diagnostics.ProcessStartInfo objProcess = new System.Diagnostics.ProcessStartInfo();
            
                objProcess.FileName = strFile;
                objProcess.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                objProcess.Verb = "print";
                objProcess.CreateNoWindow = true;
                objProcess.UseShellExecute = true;

            try
            {
                System.Diagnostics.Process.Start(objProcess);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
                        
        }

        private void Button12_Click(object sender, EventArgs e)
        {
            printDocument1.Print();
            
        }

        public void Button11_Click(object sender, EventArgs e)
        {
            if (IsInGroup("GEI Training Admins"))
            {
                string mailto = "mailto:tjbarrett@genext.com?subject=TrainingAppAddition";
                System.Diagnostics.Process.Start(mailto);
            }
            else
            {
                MessageBox.Show("You do not have permission to access this feature, please contact Jason Andre for approval.");
            }
        }

        private void Button8_Click(object sender, EventArgs e)
        {
            printDocument2.Print();
        }

        private void PrintDocument2_PrintPage(object sender, PrintPageEventArgs e)
        {
            Bitmap bm = new Bitmap(this.TblTitle_WI_XREFDataGridView.Width, this.TblTitle_WI_XREFDataGridView.Height);
            TblTitle_WI_XREFDataGridView.DrawToBitmap(bm, new Rectangle(0, 0, this.TblTitle_WI_XREFDataGridView.Width, this.TblTitle_WI_XREFDataGridView.Height));
            e.Graphics.DrawImage(bm, 0, 0);
        }

        private void Button7_Click(object sender, EventArgs e)
        {
            this.Close();
        }


    }
}
