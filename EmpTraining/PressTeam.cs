﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using Oracle.ManagedDataAccess.Client;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using System.Security.Principal;
using System.Security.Authentication;
using System.Drawing.Printing;
using System.IO;

namespace EmpTraining
{
    public partial class PressTeam : Form
    {
        string selectedValue = "";
            string[] jobTitles = { "Head Stretcher", "Billet Man", "Die Header", "Saw Man", "Press Operator" };
            string[] checkedListBoxHS =
            {
            "Capable of operating computer to view work instructions, prints, etc.",
            "Capable of using a tape measure & calipers? ",
            "Capable of updating SPC charts (when necessary)?",
            "Capable of working independently?",
            "Can adequately operate Stretcher equipment?",
            "Capable of head stretching?"
            };
            string[] checkedListBoxBM =
            {
            "Capable of operating computer to view work instructions, prints, etc.",
            "Capable of using a tape measure & calipers?",
            "Capable of updating SPC charts (when necessary)?",
            "Capable of working independently?",
            "Can adequately operate the Billet Magazine?",
            "Has received proper training for Combie Lift?"
            };
            string[] checkedListBoxDH =
            {
            "Capable of operating computer to view work instructions, prints, etc.",
            "Capable of using a tape measure & calipers?",
            "Capable of updating SPC charts (when necessary)?",
            "Capable of working independently?",
            "Can adequately operate Die Header equipment?",
            "Has received proper training for Overhead Crane?"
            };
            string[] checkedListBoxSM = {
            "Capable of operating computer to view work instructions, prints, etc.",
            "Capable of using a tape measure & calipers?",
            "Capable of updating SPC charts (when necessary)?",
            "Capable of working independently?",
            "Can adequately operate the Saw?"
            };
            string[] checkedListBoxPO =
            {
            "Capable of operating computer to view work instructions, prints, etc.",
            "Capable of operating necessary machines and/or equipment?",
            "Capable of using a tape measure & calipers?",
            "Capable of updating SPC charts (when necessary)?",
            "Capable of working independently?",
            "Can adequately operate the Press?"
            };

        public PressTeam(string value)
        {
            InitializeComponent();
            selectedValue = value;

        }

        private void Form8_Load(object sender, EventArgs e)
        {
            TextBox2.Text = selectedValue;
            TextBox1.Text = Environment.UserName.ToString();
            ComboBox2.Items.AddRange(jobTitles);
            CheckedListBox1.Hide();

        }

        private void ComboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            CheckedListBox1.Show();
            int box1;
            box1 = ComboBox2.SelectedIndex;
            switch (box1)
            { 
                case 0: 
                    CheckedListBox1.Show();
                    CheckedListBox1.Items.Clear(); 
                    CheckedListBox1.Items.AddRange(checkedListBoxHS);
                    break;
                case 1:
                    CheckedListBox1.Show();
                    CheckedListBox1.Items.Clear();
                    CheckedListBox1.Items.AddRange(checkedListBoxBM);
                    break;
                case 2:
                    CheckedListBox1.Show();
                    CheckedListBox1.Items.Clear();
                    CheckedListBox1.Items.AddRange(checkedListBoxDH);                    
                    break;
                case 3:
                    CheckedListBox1.Show();
                    CheckedListBox1.Items.Clear();
                    CheckedListBox1.Items.AddRange(checkedListBoxSM);
                    break;
                case 4:
                    CheckedListBox1.Show();
                    CheckedListBox1.Items.Clear();
                    CheckedListBox1.Items.AddRange(checkedListBoxPO);
                    break;
                    
            }
        
        }
       
        private void Button1_Click(object sender, EventArgs e)
        {
            string varDate = DateTime.Today.ToString();
            string varUser = Environment.UserName.ToString(); 
            string varNum = "";
            string varName = "";
            string checkedlb1 = "";
            string checkedlb2 = "";
            string checkedlb3 = "";
            string checkedlb4 = "";
            string checkedlb5 = "";
            string checkedlb6 = "";
            try
            {
                checkedlb1 = CheckedListBox1.GetItemChecked(0).ToString();
                checkedlb2 = CheckedListBox1.GetItemChecked(1).ToString();
                checkedlb3 = CheckedListBox1.GetItemChecked(2).ToString();
                checkedlb4 = CheckedListBox1.GetItemChecked(3).ToString();
                checkedlb5 = CheckedListBox1.GetItemChecked(4).ToString();
                checkedlb6 = CheckedListBox1.GetItemChecked(5).ToString();
            }
            catch (Exception x)
            {
                MessageBox.Show(x.ToString());
            }

            SQLConn obj = new SQLConn();
            obj.OracleConnection();
            OracleDataReader reader = obj.ODataReader("Select EMP_NUM, NAME FROM GLOVIA_PROD.EMPLOYEE WHERE CREW_FLAG <> 'Y' order by NAME");

            reader.Read();

            while (reader.Read())
            {
                try
                {
                    varNum = TextBox2.Text;
                    varNum = varNum.Substring(0, 4);
                    varName = TextBox2.Text;
                    varName = varName.Substring(9);

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }

            }
            obj.OracleCloseConnection();
            reader.Close();
        
            DataTable table3 = new DataTable();
            obj.OpenConection();

            obj.DataAdapter("Insert into tblEXT_NEW_EMP (EMP_NUM, EMP_NAME, Date_Created, Created_By, pressteamQ1, pressteamQ2, pressteamQ3, "
               + "pressteamQ4, pressteamQ5, pressteamQ6) SELECT '" + varNum + "' as EMP_NUM_Ins, '" + varName + "' as EMP_NAME_Ins, '" 
               + varDate + "' as Date_Created_Ins, '" + varUser + "' as Created_By_Ins, '" + checkedlb1 + "' as pressteamQ1_Ins, '" 
               + checkedlb2 + "' as pressteamQ2_Ins, '" + checkedlb3 + "' as pressteamQ3_Ins, '" + checkedlb4 + "' as pressteamQ4_Ins, '" 
               + checkedlb5 + "' as pressteamQ5_Ins, '" + checkedlb6 + "' as pressteamQ6_Ins")
                .Fill(table3);

            obj.CloseConnection();           

            MessageBox.Show("You've successfully input this record.");
            this.Close();

        }
    }
}
