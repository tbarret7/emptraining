﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using Oracle.ManagedDataAccess.Client;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using System.Security.Principal;
using System.Security.Authentication;
using System.Drawing.Printing;
using System.IO;

namespace EmpTraining
{
    public partial class HeavyMachinery : Form
    {
        string selectedValue = "";

        public HeavyMachinery(string value)
        {
            InitializeComponent();
            selectedValue = value;

        }

        private void Form6_Load(object sender, EventArgs e)
        {
            TextBox2.Text = selectedValue;
            TextBox1.Text = Environment.UserName.ToString();

        }

        private void Button1_Click(object sender, EventArgs e)
        {
            string varDate = DateTime.Today.ToString();
            string varUser = Environment.UserName.ToString();
            string varNum = "";
            string varName = "";
            bool var1 = CheckBox1.Checked;
            bool var2 = CheckBox2.Checked;
            bool var3 = CheckBox3.Checked;
           
            SQLConn obj = new SQLConn();
            obj.OracleConnection();
            OracleDataReader reader = obj.ODataReader("Select EMP_NUM, NAME FROM GLOVIA_PROD.EMPLOYEE WHERE CREW_FLAG <> 'Y' order by NAME");
            reader.Read();

            while (reader.Read())
            {
                try
                {
                    varNum = TextBox2.Text;
                    varNum = varNum.Substring(0, 4);
                    varName = TextBox2.Text;
                    varName = varName.Substring(9);

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }

            }

            obj.OracleCloseConnection();
            reader.Close();
                                  
            obj.OpenConection();           
            DataTable table3 = new DataTable();
            obj.DataAdapter("Insert into tblFAB_NEW_EMP (EMP_NUM, EMP_NAME, Date_Created, Created_By, towQ1, craneQ1, combieQ1) SELECT '"
           + varNum + "' as EMP_NUM_Ins, '" + varName + "' as EMP_NAME_Ins, '" + varDate + "' as Date_Created_Ins, '" + varUser + "' as Created_By_Ins, '" + var1 + "' as towQ1_Ins, '"
           + var2 + "' as craneQ1_Ins, '" + var3 + "' as combieQ1_Ins")
               .Fill(table3);
            obj.CloseConnection();            

            MessageBox.Show("You've successfully input this record.");
            this.Close();

        }

        private void Button4_Click(object sender, EventArgs e)
        {
            MessageBox.Show("This feature has not been simulated yet..");
        }
    }
}
